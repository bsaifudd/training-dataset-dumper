#!/usr/bin/env python

"""
Dump some ftag trigger info!

This runs quite a few algorithms upstream of the dumping code, to
extract trigger jets from the trigger decisions, truth label them, and
compare them to offline reconstructed jets.
"""

from BTagTrainingPreprocessing import trigger as trig
from BTagTrainingPreprocessing import mctc
from BTagTrainingPreprocessing import dumper

from AthenaConfiguration.ComponentFactory import CompFactory

import sys

def get_args():
    pflow_chain = 'HLT_j20_0eta290_020jvt_boffperf_pf_ftf_L1J15'
    emtopo_chain = 'HLT_j20_roiftf_preselj20_L1RD0_FILLED'
    dh = dict(help='(default: %(default)s)')
    parser = dumper.base_parser(__doc__)
    parser.add_argument('--pflow-chain', default=pflow_chain, **dh)
    parser.add_argument('--emtopo-chain', default=emtopo_chain, **dh)
    parser.add_argument('--mc21', action='store_true')
    parser.add_argument('--no-offline-jets', dest='offline_jets', default=True, action='store_false')
    return parser.parse_args()

def run():
    args = get_args()

    from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags

    dumper.update_cfgFlags(cfgFlags, args)
    cfgFlags.lock()

    #########################################################################
    ################### Build the component accumulator #####################
    #########################################################################
    #
    ca = dumper.getMainConfig(cfgFlags, args)

    # force both chains to pass
    dargs = dict(additional_chains=[args.pflow_chain, args.emtopo_chain])

    ca.merge(trig.pflowDumper(
        cfgFlags,
        chain=args.pflow_chain,
        offline_jets=args.offline_jets,
        **dargs
    ))
    ca.merge(trig.emtopoDumper(
        cfgFlags,
        chain=args.emtopo_chain,
        mc21=args.mc21,
        offline_jets=args.offline_jets,
        **dargs
    ))

    ca.merge(mctc.getMCTC())

    output = CompFactory.H5FileSvc(path=str(args.output))
    ca.addService(output)

    ca.addEventAlgo(CompFactory.SingleBTagAlg(
        'pflowDumper',
        output=output,
        configFileName=str(args.config_file),
        group='pflow'))
    ca.addEventAlgo(CompFactory.SingleBTagAlg(
        'topoDumper',
        output=output,
        configFileName=str(args.config_file),
        group='emtopo'))

    #########################################################################
    ########################### Run everything ##############################
    #########################################################################
    return ca.run()

if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
